const Realm = require('realm');

// Define your models and their properties
const companySchema = {
    name: 'Company',
    primaryKey: 'id',
    properties: {
        id: 'string',
        name: 'string',
        established_on: 'string',
        certified: 'string',
        logo: 'string?',
        cars: {type: 'list', objectType: 'Car'},


    }
};
// make this available to our users in our Node applications
module.exports = companySchema;

