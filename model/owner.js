const Realm = require('realm');

// Define your models and their properties
const ownerSchema = {
    name: 'Owner',
    primaryKey: 'id',
    properties: {
        id: 'string',
        name: 'string',
        address: 'string',
        hypo: 'string',
        cars: {type: 'list', objectType: 'Car'},

    }
};
// make this available to our users in our Node applications
module.exports = ownerSchema;
