const Realm = require('realm');
const imageSchema = require("../model/image");
const companySchema = require("../model/company");
const ownerSchema = require("../model/owner");
// Define your models and their properties
const carSchema = {
    name: 'Car',
    primaryKey: 'id',
    properties: {
        id: 'string',
        name: 'string',
        model: 'string?',
        color: 'string?',
        mileage: 'string?',
        companyId: 'string?',
        owners: {type: 'linkingObjects', objectType: 'Owner', property: 'cars'}
    }
};
// make this available to our users in our Node applications
module.exports = carSchema;

