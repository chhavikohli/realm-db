const carSchema = require("../model/car");
const imageSchema = require("../model/image");
const companySchema = require("../model/company");
const ownerSchema = require("../model/owner");
const databaseOptions = {
    path: 'RealmInNodeJS.realm',
    schema: [carSchema, imageSchema,companySchema ,ownerSchema],
    schemaVersion: 0, //optional
};
module.exports =databaseOptions;