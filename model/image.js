const Realm = require('realm');

// Define your models and their properties
const imageSchema = {
    name: 'Image',
    primaryKey: 'id',
    properties: {
        id: 'string',
       picture:'string?',
        carId:'string'
    }
};
// make this available to our users in our Node applications
module.exports = imageSchema;