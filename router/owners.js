const express = require('express');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './views/assets//')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
});

const upload = multer({ storage: storage });
const router = express.Router();

const ownerController = require('../controller/owners')
router.get('/getAllOwners', ownerController.get);
router.post('/addOwner', upload.single('logo'), ownerController.post);

module.exports = router;