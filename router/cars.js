const express = require('express');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './views/assets//')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
});

const upload = multer({ storage: storage });
const router = express.Router();

const carController = require('../controller/cars');
router.get('/getAllCars', carController.get);
router.post('/addCars', upload.single('picture'), carController.post);

module.exports = router;