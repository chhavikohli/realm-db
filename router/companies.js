const express = require('express');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './views/assets//')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
});

const upload = multer({ storage: storage });
const router = express.Router();

const companyController = require('../controller/companies');
router.get('/getAllCompanies', companyController.get);
router.post('/addCompany', upload.single('logo'), companyController.post);

module.exports = router;