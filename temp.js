const Realm = require('realm');

// Define your models and their properties
const CarSchema = {
    name: 'Car',
    properties: {
        make:  'string',
        model: 'string',
        miles: {type: 'int', default: 0},
    }
};
const PersonSchema = {
    name: 'Person',
    properties: {
        name:     'string',
        birthday: 'date',
        cars:     'Car[]',
        picture:  'data?' // optional property
    }
};

Realm.open({schema: [CarSchema, PersonSchema]})

    .then(realm => {
        console.log("realm open");
        // Create Realm objects and write to local storage
        realm.write(() => {
            const myCar = realm.create('Car', {
                make: 'Maruti',
                model: 'Alto',
                miles: 1000,
            });
            /*myCar.miles += 20; */// Update a property value
        });

        // Query Realm for all cars with a high mileage
        const cars = realm.objects('Car').filtered('miles <1500 ');
        console.log(cars);

        // Will return a Results object with our 1 car
        cars.length // => 1

        // Add another car
        realm.write(() => {
            console.log("realm write");
            const myCar = realm.create('Car', {
                make: 'Ford',
                model: 'Focus',
                miles: 2000,
            });
        });

        // Query results are updated in realtime
        cars.length // => 2
        realm.write(() => {
            console.log("realm write");
            const person = realm.create('Person', {
                name:     'string',
                birthday: '2/11/1996',
                cars:     CarSchema.name[23],

            });
        });

    })
    .catch(error => {
        console.log(error);
    });