const Realm = require('realm');
const utils = require("../utils/randomKeyGenerator");
const databaseOptions =require("../model/databaseOptions");


/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.get = function get(req, res, next) {
    Realm.open(databaseOptions)
        .then(realm => {
            console.log("real open")
            let allUsers = realm.objects('Company');
            realm.close();
            return res.send({status: true, data: allUsers})
        })
        .catch(error => {
            console.log(error);
        });
};


/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.post = function (req, res, next) {
    let id = utils.keyGenerator();
    let carId = utils.keyGenerator();
    let payload = {
        id: id,
        name: req.body.name,
        established_on: req.body.established_on,
        certified: req.body.certified,
    };
    if (req.file) {
        payload.logo = req.file.filename;
    }

    Realm.open(databaseOptions)
        .then(realm => {
            console.log("realm open");
            let filteredCompany = realm.objects('Company')
                .filtered(`name='${req.body.name.trim()}'`);
            if (filteredCompany.length > 0) {
                return res.send({status: false, body: {}, message: "company already exist"})
            }
            // Create Realm objects and write to local storage
            realm.write(() => {
                const company = realm.create('Company', payload);
                if (req.body.carName) {
                    company.cars.push({
                            id: carId,
                            name: req.body.carName,
                            model: req.body.carModel,
                            color: req.body.carColor,
                            mileage: req.body.carMileage,
                            companyId:id
                        }
                    );
                }

            });
            realm.close();
            return res.send({status: true, data: payload, message: "company added successfully"})
        })
        .catch(error => {
            console.log(error);
        });

};

















