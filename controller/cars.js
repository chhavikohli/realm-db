const Realm = require('realm');
const utils =require("../utils/randomKeyGenerator");
const databaseOptions =require("../model/databaseOptions");


/**
 *
 * @param req
 * @param res
 * @param next
 */

exports.get = function get(req, res, next) {
    Realm.open(databaseOptions)
        .then(realm => {
            console.log("real open")
            let allcars = realm.objects('Car');
            realm.close();
            return res.send({status: true ,data:allcars})
        })
        .catch(error => {
            console.log(error);
        });
};

/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.post = function (req, res, next) {

    let id = utils.keyGenerator();
    let imageId = utils.keyGenerator();
    let picture ='';
    let payload = {
        id: id,
        name: req.body.name,
        model: req.body.model,
        color: req.body.color,
        mileage: req.body.mileage,
    };
    if (req.file) {
        picture = req.file.filename;
    }

    Realm.open(databaseOptions)
        .then(realm => {
            console.log("realm open");
            let filteredCompany = realm.objects('Company')
                .filtered(`name='${req.body.company.trim()}'`);
            console.log(filteredCompany);
            if(!filteredCompany[0]){
              return res.send({status:false,data:{},message:'company not registered ...'})
            }
            payload.companyId=filteredCompany[0].id;

                realm.write(() => {
                    let user = realm.objectForPrimaryKey('Company', filteredCompany[0].id)
                    user.cars.push(payload);

                });

           /* realm.write(() => {
                const car = realm.create('Car', payload);
            });*/
            if(picture){
                realm.write(() => {
                    const image = realm.create('Image',{
                        id:imageId,
                        picture:picture,
                        carId:id,

                    });
                });
            }
            realm.close();
           return res.send({status: true, data: payload, message: "car added successfully"})
        })
        .catch(error => {
            console.log(error);
        });

};

















