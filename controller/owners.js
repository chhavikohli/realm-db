const Realm = require('realm');
const utils = require("../utils/randomKeyGenerator");
const databaseOptions =require("../model/databaseOptions");

/**
 *
 * @param req
 * @param res
 * @param next
 */

exports.get = function get(req, res, next) {
    Realm.open(databaseOptions)
        .then(realm => {
            console.log("real open")
            let allowners = realm.objects('Owner');
            realm.close()
            return res.send({status: true, data: allowners})
        })
        .catch(error => {
            console.log(error);
        });
};


/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.post = function (req, res, next) {

    let id = utils.keyGenerator();
    let carId = utils.keyGenerator();
    let payload = {
        id: id,
        name: req.body.name,
        address: req.body.address,
        model: req.body.model,
        hypo: req.body.hypo,
    };

    Realm.open(databaseOptions)
        .then(realm => {
            console.log("realm open");


            /*----------------------------------adding owner-relation to car schema----------------------------------------------------------------*/
/*

            let filteredCar = realm.objects('Car')
                .filtered(`name='${req.body.carName.trim()}'`);
            if (filteredCar.length > 0) {
                realm.write(() => {
                    let car = realm.objectForPrimaryKey('Car', filteredCar[0].id);
                    console.log('-------adding owner-relation to car schema--- existing car-----',filteredCar);

                    console.log('-----all owners of this car----------',car.owners);
                   /!* car.owners.Results.push(payload);*!/
                    console.log('----- update all owners of this car----------',car.owners[0]);
                });
            }
*/


            /*-------------------------------If owner already exist that add new car to same owner in car[]-----------------------------------------------------------------------------------------*/


            let filteredOwner = realm.objects('Owner')
                .filtered(`name='${req.body.name.trim()}'`);
            if (filteredOwner.length > 0) {
                realm.write(() => {
                    let user = realm.objectForPrimaryKey('Owner', filteredOwner[0].id);
                    user.cars.push({
                            id: carId,
                            name: req.body.carName,
                            model: req.body.carModel,
                            color: req.body.carColor,
                            mileage: req.body.carMileage
                        }
                    );
                });
                return res.send({status: true, data: {}, message: `${filteredOwner[0].id} updated`})
            }

            /*----------------------------------------adding new owner and car[]----------------------------------------------------------------------------------------*/


            realm.write(() => {
                const owner = realm.create('Owner', payload);
                if (req.body.carName) {
                    owner.cars.push({
                            id: carId,
                            name: req.body.carName,
                            model: req.body.carModel,
                            color: req.body.carColor,
                            mileage: req.body.carMileage,
                        }
                    );
                }

            });
            realm.close();
            return res.send({status: true, data: payload, message: "owner added successfully"})
        })
        .catch(error => {
            console.log(error);
        });
};

















