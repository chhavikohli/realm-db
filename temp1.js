const ShipSchema = {
    name: 'Ship',
    properties: {
        captain: 'Captain'
    }
}

const CaptainSchema = {
    name: 'Captain',
    properties: {
        ships: {type: 'linkingObjects', objectType: 'Ship', property: 'captain'}
    }
}

let captain = realm.objectForPrimaryKey('Captain', 1);
let ships = captain.linkingObjects('Ship', 'captain');