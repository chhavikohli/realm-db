const express = require('express');

const companies = require('./router/companies');
const cars = require('./router/cars');
const owners = require('./router/owners');
const bodyParser = require('body-parser');

const app = express();

app.use('/static', express.static('views'));
app.use(bodyParser.json());

app.use('/', companies);
app.use('/', cars);
app.use('/', owners);

app.listen('8300', function () {
    console.log('application started at 8300');
});
